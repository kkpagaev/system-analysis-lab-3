Matrix size: 150
multiply time taken: 0.0240775s
multiplyParallel time taken: 0.0600354s
Speedup: 0.40105504419059423

Matrix size: 300
multiply time taken: 0.0562715s
multiplyParallel time taken: 0.06496s
Speedup: 0.866248460591133

Matrix size: 450
multiply time taken: 0.2130777s
multiplyParallel time taken: 0.1085203s
Speedup: 1.9634824083604636

Matrix size: 600
multiply time taken: 0.3663179s
multiplyParallel time taken: 0.1791836s
Speedup: 2.044371806348349

Matrix size: 750
multiply time taken: 1.1128914s
multiplyParallel time taken: 0.4099699s
Speedup: 2.7145685573501863

Matrix size: 900
multiply time taken: 1.4944303s
multiplyParallel time taken: 0.5102572s
Speedup: 2.9287784670162424

Matrix size: 1050
multiply time taken: 3.7110226s
multiplyParallel time taken: 1.3403029s
Speedup: 2.76879397933109

Matrix size: 1200
multiply time taken: 5.3556068s
multiplyParallel time taken: 2.0128265s
Speedup: 2.6607394129598356

Matrix size: 1350
multiply time taken: 8.8204896s
multiplyParallel time taken: 3.7294376s
Speedup: 2.3650991237928207

Matrix size: 1500
multiply time taken: 15.3170588s
multiplyParallel time taken: 5.9779611s
Speedup: 2.5622546791078986

Matrix size: 2000
multiply time taken: 68.0675879s
multiplyParallel time taken: 18.5309767s
Speedup: 3.6731786457861126

Matrix size: 2500
multiply time taken: 183.1825892s
multiplyParallel time taken: 41.2116515s
Speedup: 4.444922310381082

Matrix size: 3000
multiply time taken: 9.349513s
multiplyParallel time taken: 3.8824634s
Speedup: 2.408139378725373

Matrix size: 1500
multiply time taken: 14.9890985s
multiplyParallel time taken: 6.6600882s
Speedup: 2.2505855853380443

Matrix size: 2000
multiply time taken: 70.7625193s
multiplyParallel time taken: 19.7494252s
Speedup: 3.5830166490111313
