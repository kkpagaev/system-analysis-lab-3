public class App {
    public static void main(String[] args) throws Exception {
        int sizes[] = { 150, 300, 450, 600, 750, 900, 1050, 1200, 1350, 1500, 2000 };
        for (int size : sizes) {
            int[][] matrix1 = generateMatrix(size, size);
            int[][] matrix2 = generateMatrix(size, size);
            System.out.println("Matrix size: " + size);
            double s1 = measureExecutionTime(() -> multiply(matrix1, matrix2));
            System.out.println("multiply time taken: " + s1 + "s");
            double s2 = measureExecutionTime(() -> multiplyParallel(matrix1, matrix2));
            System.out.println("multiplyParallel time taken: " + s2 + "s");
            System.out.println("Speedup: " + s1 / s2);
            System.out.println();
        }
    }

    private static int[][] generateMatrix(int i, int j) {
        int[][] matrix = new int[i][j];
        for (int k = 0; k < i; k++) {
            for (int l = 0; l < j; l++) {
                matrix[k][l] = (int) (Math.random() * 100);
            }
        }
        return matrix;
    }

    public static double measureExecutionTime(Runnable runnable) {
        long startTime = System.nanoTime();
        runnable.run();
        long endTime = System.nanoTime();
        long duration = (endTime - startTime);
        double seconds = (double) duration / 1_000_000_000.0;
        return seconds;
    }

    public static int[][] multiply(int a[][], int b[][]) {
        int c[][] = new int[a.length][b[0].length];
        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < b[0].length; j++) {
                for (int k = 0; k < a[0].length; k++) {
                    c[i][j] += a[i][k] * b[k][j];
                }
            }
        }
        return c;
    }

    public static int[][] multiplyParallel(int a[][], int b[][]) {
        int c[][] = new int[a.length][b[0].length];
        Thread[] threads = new Thread[a.length];
        for (int i = 0; i < a.length; i++) {
            threads[i] = new Thread(new RowMultiplyWorker(c, a, b, i));
            threads[i].start();
        }
        for (int i = 0; i < a.length; i++) {
            try {
                threads[i].join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return c;
    }

    public static void printMatrix(int a[][]) {
        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < a[i].length; j++) {
                System.out.print(a[i][j] + " ");
            }
            System.out.println();
        }
    }
}

class RowMultiplyWorker implements Runnable {
    private int[][] c;
    private int[][] a;
    private int[][] b;
    private int row;

    public RowMultiplyWorker(int[][] c, int[][] a, int[][] b, int row) {
        this.c = c;
        this.a = a;
        this.b = b;
        this.row = row;
    }

    @Override
    public void run() {
        for (int j = 0; j < b[0].length; j++) {
            for (int k = 0; k < a[0].length; k++) {
                c[row][j] += a[row][k] * b[k][j];
            }
        }
    }
}